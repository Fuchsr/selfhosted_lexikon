## Wörterbuch und Informationen des Selfhosted DACH Chats
![GitHub](https://img.shields.io/github/license/fuxsr/dictionary_selfhosted) ![GitHub](https://img.shields.io/badge/made-in_DACH-brightgreen)



Die aktuelle Version in Webform finden Sie auf: [wiki.fuchsr.de](https://wiki.fuchsr.de/)

Dieses Projekt wurde mit [MkDocs](https://www.mkdocs.org/) erstellt.
