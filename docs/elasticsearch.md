# Elastic Search
> Für Ubuntu oder [Debian-Deriviate](https://distrowatch.com/search.php?ostype=All&category=All&origin=All&basedon=Debian&notbasedon=None&desktop=All&architecture=All&package=All&rolling=All&isosize=All&netinstall=All&language=All&defaultinit=All&status=Active)

## 1. Installation von ES (Elastic Search)
```
apt install openjdk-11-jre
```
```
apt install apt-transport-https
```
```
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
```
```
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | tee -a /etc/apt/sources.list.d/elasticsearch.list
```
```
apt update
```
```
apt install elasticsearch
```
> apt install elasticsearch=6.8.3 (stable version for nextcloud)

### Elasticsearch config
```
vim /etc/elasticsearch/elasticsearch.yml
```
> Falls du keine Erfahrung mit VIM hast:
> * drücke i um in den Einfügen Modus zu wechseln
> * ESC um wieder in den normalen Modus zu gelangen
> * tippe im normalen Modus :x um die Datei zu speichern und VIM zu verlassen
> * oder :q! um VIM zu beenden ohne zu speichern

Verändere den Wert `network.host: 127.0.0.1`

### Registrierung und starten des ES Service
```
sudo systemctl daemon-reload && sudo systemctl enable --now elasticsearch
```
---
### Üperprüfen ob ES läuft
`systemctl status elasticsearch`
```
curl -XGET '127.0.0.1:9200/?pretty'
```

Falls der Service nicht gestartet werden konnte ist eventuell nicht genügend Arbeitsspeicher frei.
In diesem Fall kannst Du den Xmx Wert in `/etc/elasticsearch/jvm.options` auf z.B. verringern 512M.

## 2. Installiere „ingest-attachment“

Installiere das „ingest-attachment“ Plugin (benötigt für PDF, PPT, XLS etc.)
```
sudo /usr/share/elasticsearch/bin/elasticsearch-plugin install ingest-attachment
```
Nach erfolgreicher Installation, starte ES neu:

```
sudo systemctl restart elasticsearch
```



## 3. (optional) Installiere Tesseract OCR

Falls benötigt installiere nun OCR (optical character recognition, Optische Texterkennung auf Deutsch)

```
sudo apt install tesseract-ocr tesseract-ocr-eng tesseract-ocr-deu tesseract-ocr-fra
```

### (optional) Füge mehr Sprachen für OCR hinzu

Du kannst zusätzliche Sprachen für das OCR Plugin mit diesem Befehl installieren:
```
sudo apt install tesseract-ocr tesseract-ocr-all
```
